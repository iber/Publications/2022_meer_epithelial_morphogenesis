# **2022_Meer_Epithelial_Morphogenesis**

M. Meer, R. Vetter, A. Iannini, G. Villa-Fombuena, H. Gómez, L. Hodel, F. Casares, D. Iber, _Minimisation of surface energy drives apical epithelial organisation and gives rise to Lewis’ law and Aboav-Weaire’s law_.

# Supplementary Material

## [Data](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/data)
This folder contains image and segmentation data of the following tissues:
* dPE (*Drosophila* eye disc peripodial membrane), 
* dWL (*Drosophila* larval wing disc) and
* dPW (*Drosophila* pupal wing disc). 



## Analysis scripts

### [Apical cell areas and polygon types](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/analysis_scripts/area_and_polygon_type)
* This folder contains an R script which batch processes XLS files that were created by the [ICY](http://icy.bioimageanalysis.org) plugin [EpiTools](https://epitools.github.io).
 
### [**_m<sub>n</sub>_**](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/analysis_scripts/m_n)
* This folder contains an R script with which you can extract the average number of neighbours _m<sub>n</sub>_ of the n<sub>i</sub> neighbours of a cell with _n_ neighbours.
* As an input you need the XLS output file that you created with the [modified](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/source_codes/EpiTools_modification) Icy plugin EpiTools.

### [Sidelengths and internal angles](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/analysis_scripts/sidelengths_and_internal_angles)
* This folder contains R scripts with which you can extract the internal angles and sidelengths of segmented cells from an SQL database *TA.db* created by the Fiji plugin TissueAnalyzer.
* Installation instructions for TissueAnalyzer can be found [here](https://github.com/mpicbg-scicomp/tissue_miner/blob/master/MovieProcessing.md).
* In TissueAnalyzer, the database *TA.db* is created in the following way:
    - As an input, you need a 2D microscopy image of visible cell boundaries, ideally a binary image (white boundaries, black background).
    - TissueAnalyzer can be used to create such a segmentation using the *Detect Bonds* functions in the *Segmentation* tab of the plugin. 
    - Manual curation of the segmentation is a valuable feature of the plugin and is highly recommended.
    - Assuming you have such a curated segmentation, you can create the database by selecting *Finish all* in the tab *PostProcess*.



## Source codes

### [LBIBCell](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/source_codes/LBIBCell)
* In this folder you find the BioSolvers that were created in addition to [LBIBCell](https://tanakas.bitbucket.io/lbibcell/index.html)'s default BioSolvers. 

### [Analytical approximation](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/source_codes/analytical_approximation)
* This folder contains MATLAB scrips and functions that allow to evaluate and fit the theoretical approximation to the Aboav-Weaire law developed in the Supplementary Material. 
* The fitting functionality requires the `fitnlm` function from MATLAB's Statistics Toolbox.

### [Prediction](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/source_codes/Prediction)

- This folder contains a MATLAB script to predict how the fraction of hexagons depends on the width of the area distribution, if the areas follow a lognormal distribution and the mean areas of each
polygon type either follow the linear Lewis law or the quadratic relationship.

### [EpiTools_modification](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/tree/main/source_codes/EpiTools_modification) 
* In this folder you find the modified JAR file that belongs to the Icy plugin EpiTools. 
* The modification adds the IDs of the _n<sub>i</sub>_ neighbours of a cell as an additional column in the XLS output file created by EpiTools' *CellExport* function.
* The output file can be further processed using the scripts in the subsequent pipeline folders.
* Installation instruction: Just replace this file in the plugin folder. 
* Installation instructions for the Icy plugin EpiTools can be found [here](https://epitools.github.io/wiki/Icy_Plugins/00_Installation/).

## License
All source code and data in this repository is released under the 3-Clause BSD license (see [LICENSE](https://git.bsse.ethz.ch/iber/Publications/2022_meer_epithelial_morphogenesis/-/blob/main/LICENSE) for details).
