## Purpose of script: Takes multiple samples in EpiTools XLS output format,
##                    calculates polygon frequencies (per sample), 
##                    normalizes cell areas (per sample)
##                    and merges all samples into single dataset.   
##
## Copyright (c) Marco Kokic, 2019
## Email: marco.kokic@bsse.ethz.ch


# Place script in parent directory
setwd(getwd())

perl <- "C:/Strawberry/perl/bin/perl5.28.1.exe" # Needed in Windows
library(gdata)
library(plyr)

# Get all EpiTools output files (representing samples) located in 'directory'
directory = "./EYEDISC_PP/age10" 
EpiTools_files = list.files(pattern=".xls", path = directory, recursive =TRUE, full.names = T)

# Function to obtain polygon frequencies
get.table <- function(DF,variable){
  DF_table <- as.data.frame(table(DF[variable]))
  names(DF_table)=c(variable,"N")
  DF_table$Freq = DF_table$N/sum(DF_table$N)
  return(DF.table)
}

# Function to extract information from EpiTools output files
get.xls <- function(xls){
  raw = read.xls(xls, perl=perl)
  raw = subset(raw, raw$onSegmentationBoundary ==FALSE) # exclude boundary cells
  raw = subset(raw, raw$polygonNo > 2) # only include real polygons
  raw$area.norm = raw$area / sum(raw$area) # additional column with normalized areas
  #Alternatively, normalize by average area of hexagons
  #raw$area.norm = raw$area / sum(raw$area[raw$polygonNo==6])
  df_pd  = get.table(raw,"polygonNo") # get frequencies of polygons
  df = Reduce(merge,list(raw,df_pd)) # merge both dataframes
  return(df)
}

# Function to convert a list of dataframes to a single dataframe
rbinder <- function(list_of_DFs){
  df <- data.frame()
  for(i in 1:length(list_of_DFs)){
    df <- rbind.fill(df,list_of_DFs[[i]])
  }
  return(df)
}

# Apply functions to batch of EpiTools output files located in 'directory'
batch_process <- function(EpiTools_files){
  list_of_DFs = lapply(EpiTools_files, get.xls)
  # Add column with sample ID 
  list_of_DFs <- Map(cbind,list_of_DFs,sample=1:length(agelist))
  df <- rbinder(list_of_DFs)
  return(df)
}

DF <- batch_process(EpiTools_files)
write.csv(DF,"./EpiTools_processed_all_samples.csv",row.names=FALSE)
