% Given vectors n and m*n, fits the parameters theta_high and beta
% assuming theta_low=0 and alpha=beta.
function model = fit_irregular(n, mn)

model = fitnlm(n, mn, @(param,n) m_irregular_numeric([0 param(1) param(2) param(2)],n).*n, [150 0.6]);
    
end