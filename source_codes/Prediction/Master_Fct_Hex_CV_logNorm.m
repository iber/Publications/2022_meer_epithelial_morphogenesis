function Master_Fct_Hex_CV_logNorm()
% This function determines the hexagon fraction dependent on the  area CV
% if the areas follow a lognormal distribution and the mean areas of each
% polygon type either follow the linear LL or the quadratic relationship.
% The results are saved for plotting with plot_Hex_CV.m.
% -------------------------------------------------------------------------
clear all,
close all,
% -------------------------------------------------------------------------
% 1. define mean relative area (mu) and area CV
% range of CV values to be explored
CV_min = 0.05;
CV_max = 1.1;  % maximal reported area CV < 1.1
N_CV = 106; % number of CV values 
CV_values = linspace(CV_min, CV_max, N_CV);
mu = CV_values./CV_values; % normalised mean area for each CV value

CV_plotting = 0.4; % area distribution to be plotted externally
[~,CV_plot_index] = min(abs(CV_values-CV_plotting')); % get index of CV value closest to CV_plotting

log_std = CV_values.*mu; % standard deviation 
mu_log = log(mu.^2./sqrt(log_std.^2+mu.^2)); % transform mean & std to lognormal
std_log = sqrt(log(log_std.^2./mu.^2 + ones(1, length(log_std)))); % transform mean & std to lognormal
% -------------------------------------------------------------------------
% 2. mean(A_n) according to linear LL or the quadratic relationship:
for n = 3:12
    An(n) = (n/6)^2; % quadratic relationship
    An_LL(n) = (n-2)/4; % linear LL
end
% ----------------------------------------------------------------------------------------
% 3. Initial guess for left limits of A_n in the area distribution for each neighbour number n 
limit(1:3) = 0; % polygons are at least triangles
limit_LL(1:3) = 0; % polygons are at least triangles
for n = 3:11 % place the limits in the middle between An
    limit(n+1) = (An(n+1)+An(n))/2;
    limit_LL(n+1) = (An_LL(n+1)+An_LL(n))/2;
end
% alternatively, use optimised limits for CV = 0.05 to speed up optimisation:
limit(4:7)    = [0.25     0.445     0.71      1.3];
limit_LL(4:7) = [0.25     0.5       0.75      1.3];
% differences between limits
diff_limit = limit(2:length(limit))-limit(1:length(limit)-1);
diff_limit_LL = limit_LL(2:length(limit_LL))-limit_LL(1:length(limit_LL)-1);
% -------------------------------------------------------------------------
% 4. Optimisation
% Determine the left limits of A_n in the area distribution ('LogNormal') 
% for each neighbour number n such that mean(A_n) best fits the 
% linear LL (An_LL) or the quadratic relationship (An)
[p_QR_log, n_limits_plotting_log]    = optimisation_routine_dist(diff_limit,    mu_log, std_log, An,    CV_plot_index, 'LogNormal'); % quadratic relationship
[p_LL_log, n_limits_plotting_LL_log] = optimisation_routine_dist(diff_limit_LL, mu_log, std_log, An_LL, CV_plot_index, 'LogNormal'); % linear LL
%-------------------------------------------------------------------------
% 5. SAVE RESULTS for external plotting
save_matrix_to_file([[0, CV_values]', [1, p_QR_log]', [1, p_LL_log]'], '', 'CV_Hex') % CV and hexagon fractions
save_matrix_to_file(CV_values(CV_plot_index), '', 'CV_plotting') % save CV value of An limits
save_matrix_to_file([n_limits_plotting_log', n_limits_plotting_LL_log'], '', 'An_limits') % An limits
%-------------------------------------------------------------------------
end % end Master routine
%% ------------------------------------------------------------------------




%% ------------------------------------------------------------------------
%%                              SUBFUNCTIONS
%% ------------------------------------------------------------------------


%% ------------------------------------------------------------------------
function [p_hexagons, n_limits_plotting] = optimisation_routine_dist(limit, mu, CV_values, An, CV_plot_index, distribution)
% -------------------------------------------------------------------------
% Determine the left limits of A_n in the area distribution 
% for each neighbour number n such that mean(A_n) best fits the 
% linear LL (An_LL) or the quadratic relationship (An)

%% Input
% limit: initial guess of A_n limits
% mu: mean of normalised area distribution
% CV_values: CV of normalised area distribution
% An: mean normalised area of each polygon type according to quadratic
%       relationship or LL
% CV_plotting: to be plotted CV of normalised area distribution
% distribution: 'LogNormal'

%% Output
% p_hexagons: fraction of hexagons
% n_limits_plotting: inferred A_n limits for CV_plotting

%% Function
% Standard settings for optimisation procedure
options = optimoptions('fsolve','Algorithm','Levenberg-Marquardt', ...
    'MaxFunctionEvaluations', 1e5, 'MaxIterations', 1e5);

% Advanced Settings
options = optimoptions('fsolve','Algorithm','Levenberg-Marquardt', ...
    'MaxFunctionEvaluations', 1e6, 'MaxIterations', 1e6, ...
    'FunctionTolerance', 1e-9, 'StepTolerance', 1e-9);
% StepTolerance is a lower bound on the size of a step, meaning the norm of (xi – xi+1). If the solver attempts to take a step that is smaller than StepTolerance, the iterations end. StepTolerance is generally used as a relative bound, meaning iterations end when |(xi – xi+1)| < StepTolerance*(1 + |xi|), or a similar relative measure.
% FunctionTolerance is generally used as a relative bound, meaning iterations end when |f(xi) – f(xi+1)| < FunctionTolerance*(1 + |f(xi)|), or a similar relative measure.

for index =1:length(CV_values) % loop over all CV values    
    clear bfound2 p_polygons n_limits
    % ---------------------------------------------------------------------
    [bfound2, ~, ~] = fsolve(@(x)func2minimize_dist(x, An, ...
            mu(index), CV_values(index), distribution), limit, options);
        
    % use the inferred values as starting guess for next CV value optimisation    
    limit = bfound2;    
        
    % convert to left An limits
    n_limits(1:2) = 0; % no polygons with n<3
    for i=2:length(bfound2)
        n_limits(i+1) = n_limits(i) + abs(bfound2(i));
    end
    
    % return left A_n limits at one CV value
    if index == CV_plot_index 
        n_limits_plotting = n_limits;
    end
    
    % calculate the fraction of hexagons by getting the cdf difference
    % between the limit of pentagon/hexagon and hexagon/heptagon
    if isreal(n_limits(6:7)) == 1
        p_hexagons(index) = cdf(distribution,n_limits(7),mu(index),CV_values(index)) ...
                -cdf(distribution,n_limits(6),mu(index),CV_values(index));
    else
        p_hexagons(index) = NaN;
    end
    
    % ---------------------------------------------------------------------
    %                   MONITORING
    % ---------------------------------------------------------------------
    
    % monitor optimisation steps
    [An_model, p_polygons, n_means] = ...
        eval_result(bfound2, mu(index), CV_values(index), distribution);
    
    % An has not been determined when polygon frequency is zero
    ind = find(p_polygons == 0);
    An_model(ind) = NaN;
          
    % transform back to CV values
    % mu_norm = exp(mu(index)+CV_values(index)^2/2);
    sigma = sqrt(exp(2*mu(index)+CV_values(index)^2)*(exp(CV_values(index)^2)-1));
      
    fprintf(strcat('OUTPUT', '\n', ...
        'Distribution:', distribution, '\t', ...
    'sigma=', num2str(sigma), '\n', ...
    'Polygon Type  = \t', num2str(linspace(3,length(An),length(An)-2)), '\n', ...
    'Left limits   = \t', num2str(round(n_limits(2:end),   2)), '\n', ...
    'Inferred An   = \t', num2str(round(An_model(3:end),   2)), '\n', ...
    'Model    An   = \t', num2str(round(An(3:end),         2)), '\n', ...
    'poly Fraction = \t', num2str(round(p_polygons(3:end), 2)), '\n',  ...
    'Hexagon Fraction =', num2str(p_polygons(6)), '\n',  ...
    'mean(n)=', num2str(n_means), '\n'));
end
end


%% -------------------------------------------------------------------------
function output = func2minimize_dist(x, An, mu, sigma, distribution)
% This function returns evaluation of objective function 
% output = sum_n (A_opt(n)/A_n -1)^2 + (mean(n)-6)^2 is zero if:
% 1) A_opt(n)/A_n = 1 for each n
% 2) mean(n) = 6

%% Input
% x: A_n limits
% An: mean normalised area of each polygon type according to quadratic
%       relationship or LL
% mu: mean of normalised area distribution
% sigma: std of normalised area distribution
% distribution: 'LogNormal'

%% Output
% output: evaluation of objective function 

%% Function
[mean_An, p_polygons, mean_n] = eval_result(x, mu, sigma, distribution);

% dev_A: deviation of inferred A_n from model A_n (0 if equal)
ind = find(p_polygons>0); % only use those for which normalisation was possible
dev_A = mean_An(ind)./An(ind)-1; % zero if mean_An == An

clear dev_A2;
dev_A2 = 0;
for i=1:length(ind)
    dev_A2 = dev_A2 + p_polygons(ind(i))*dev_A(i)^2;
end

% OBJECTIVE FUNCTION
% 1st term: squared deviation of inferred A_n from model A_n, weighted by
%           polygon frequency
% 2nd term: squared deviation of mean polygon number from 6 
output = dev_A2 + (mean_n-6).^2;
% -------------------------------------------------------------------------

%% Monitor optimisation steps
monitoring = 0;
if monitoring == 1
    % transform back to CV values
    sigma = sqrt(exp(2*mu+sigma^2)*(exp(sigma^2)-1));
        
    fprintf(strcat(distribution, '\t','Distribution:', '\t', ...
    'sigma=', num2str(sigma), '\n', ...
    'An    =', num2str(mean_An), '\n', ...
    'dev_An=', num2str(dev_A), '\n', ...
    'dev_A2=', num2str(dev_A*dev_A'), '\n', ...
    'dev_mn=', num2str((mean_n-6).^2),'\n',  ...
    'HF=', num2str(p_polygons(6)),'\n',  ...
    'mean(n)=', num2str(mean_n),'\n'));
end
% -------------------------------------------------------------------------
end



function [mean_An, p_polygons, mean_n] = eval_result(x, mu, sigma, distribution)
% This function evaluates A_n and the polygon frequencies for the specified
% distribution and limits

%% Input
% x: A_n limits
% mu: mean of normalised area distribution
% sigma: std of normalised area distribution
% distribution: 'LogNormal'

%% Output
% mean_An: inferred mean area of each polygon tuype
% p_polygons: inferred polygon distribution
% mean_n: mean polygon number

% -------------------------------------------------------------------------
%% Function
% -------------------------------------------------------------------------
% create A_n boundaries
limit(1:2) = 0; % no polygons with n<3
for i=2:length(x)
    limit(i+1) = limit(i) + abs(x(i));
end
% -------------------------------------------------------------------------
% determine polygon frequencies and mean(n) based on A_n boundaries
clear p_polygons
p_polygons(1:2) = 0;
for i=3:length(limit)-1
    p_polygons(i) = cdf(distribution,limit(i+1),mu,sigma)-cdf(distribution,limit(i),mu,sigma);
end
mean_n = p_polygons*linspace(1,length(p_polygons), length(p_polygons))';
% -------------------------------------------------------------------------
% Determine mean(A_n) by integrating over normFun_exp bewteen polygon limits 
% and normalise by integrating over normFun 
if strcmp(distribution, 'LogNormal') == 1 % lognormal pdf * Area (x)
    Fun_pdf_An = @(x) 1./(sqrt(2*pi)*sigma*x).*x.*exp(-(log(x)-mu).^2./(2*sigma^2));
else
    display('Error: Distribution not supported');
end
% -------------------------------------------------------------------------
% determine mean area of each polygon type
clear mean_An
mean_An(1:2) = 0; % polygons with less than 3 vertices do not have an area
for i=3:length(limit)-1
    % integral(fun,xmin,xmax) numerically integrates function fun from xmin 
    % to xmax using global adaptive quadrature and default error tolerances.
    if p_polygons(i) > 0
        mean_An(i) = integral(Fun_pdf_An,limit(i),limit(i+1))/p_polygons(i);
    else
        mean_An(i) = 0;
    end
end

%% Monitor evaluation
monitoring = 0;
if monitoring == 1
    % transform back to CV values
    sigma = sqrt(exp(2*mu+sigma^2)*(exp(sigma^2)-1));
        
    fprintf(strcat(distribution, '\t','Distribution:', '\t', ...
    'sigma=', num2str(sigma), '\n', ...
    'Left limits = \t', num2str(limit), '\n', ...
    'polygon p   = \t', num2str(p_polygons),'\n',  ...
    'An          = \t', num2str(mean_An), '\n', ...
    'mean(n)=', num2str(mean_n),'\n'));
end
% -------------------------------------------------------------------------
end

function save_matrix_to_file(y, fld, filename)
% save matrix y to file

%% Input
% y: Matrix [CV, hexagon fraction quadratic relationship, hexagon fraction Lewis Law]
% fld: string with folder address
% filename: string with filename

fmt = [repmat('%f ', 1, size(y,2)-1), '%4d\n'];
fid = fopen(strcat(fld, filename, '.txt'), 'w');
fprintf(fid, fmt, y');
fclose(fid);
end
